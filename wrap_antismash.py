#!/usr/bin/env python
# Copyright (C) 2010, 2011 Kai Blin <kai.blin@biotech.uni-tuebingen.de>
# University of Tuebingen
# Division of Microbiology/Biotechnology
# Interfaculy Institute of Microbiology and Infection Medicine
## License: GNU General Public License v3 or later
## A copy of GNU GPL v3 should have been included in this software package in LICENSE.txt.

import sys
import os
from glob import glob
from os import path
from tempfile import mkdtemp
from shutil import copytree, copy2, move, rmtree
from zipfile import ZipFile, ZIP_DEFLATED
import subprocess

ANTISMASH_TEMPLATE=path.abspath(path.join(path.dirname(__file__), "antismash_orig"))

def zip_dir(dir_path, archive, prefix_to_remove=""):
    """Recursively add a directory's contents to a zip archive"""
    entries = os.listdir(dir_path)
    for entry in entries:
        entry = path.join(dir_path, entry)
        if path.isdir(entry):
            zip_dir(entry, archive, prefix_to_remove)
        else:
            arcname = entry.replace(prefix_to_remove + os.sep, "", 1)
            archive.write(entry, arcname)

def zip(dir_path, name):
    """Create a zip file for a given path"""
    archive = ZipFile(name, 'w', ZIP_DEFLATED)
    if path.isdir(dir_path):
        zip_dir(dir_path, archive, path.dirname(dir_path))
    else:
        archive.write(dir_path)
    archive.close()

def fixup_filename(name):
    ret = name.replace(" ", "_")
    ret = ret.replace("\t", "_")
    ret = ret.replace("(", "")
    ret = ret.replace(")", "")
    ret = ret.replace("[", "")
    ret = ret.replace("]", "")
    ret = ret.replace("/", "")
    ret = ret.replace("#", "")
    ret = ret.replace(";", "")
    ret = ret.replace("*", "")
    ret = ret.replace("\\", "")
    ret = ret.replace("~", "")
    root, ext = path.splitext(ret)
    root = root.replace(".", "")
    ext = ext.lower()
    ret = "%s%s" % (root, ext)
    return ret

def main():
    """Wrap the antismash script to deal with the file clutter"""
    args = sys.argv

    if not path.isfile(args[1]):
        print >>sys.stderr, "'%s' isn't a file, probably the download from NCBI failed." % args[1]
        sys.exit(2)

    upload_dir = path.dirname(args[1])
    genome_file = fixup_filename(path.basename(args[1]))
    tmpdir = mkdtemp(prefix='antismash-')
    file_list = glob(path.join(ANTISMASH_TEMPLATE, "*"))
    for entry in file_list:
        # skip the git directory
        if path.basename(entry) == ".git":
            continue
        dst = path.join(tmpdir, path.basename(entry))
        if path.isdir(entry):
            copytree(entry, dst)
        else:
            copy2(entry, dst)
    copy2(args[1], path.join(tmpdir, genome_file))

    args[0] = "./antismash.py"
    args[1] = genome_file
    env = os.environ
    cluseanroot = path.join(tmpdir, 'clusean')
    env['CLUSEANROOT'] = cluseanroot
    env['PERL5LIB'] = cluseanroot
    env['PATH'] += ":%s:%s" % (path.join(cluseanroot, 'bin'),
                              path.join(cluseanroot, 'scripts'))
    env['NRPS2BASEDIR'] = path.join(tmpdir, 'NRPSPredictor2')
    proc = subprocess.Popen(args, cwd=tmpdir, env=env, stderr=subprocess.PIPE)
    (stdout_data, stderr_data) = proc.communicate()
    if proc.returncode != 0:
        print >>sys.stderr, "calling antismash failed (%s): %s" % (proc.returncode,
                                                                   stderr_data)
    else:
        try:
            print "job finished, stderr is %s" % stderr_data
            resultsfh = open(path.join(tmpdir, 'resultsfolder.txt'), 'r')
            results = resultsfh.readline().strip()
            resultsfh.close()
            resultsdir = path.join(tmpdir, results)
            all_results = glob(path.join(resultsdir, "*"))

            for entry in all_results:
                if not path.exists(path.join(upload_dir, path.basename(entry))):
                    move(entry, upload_dir)
            zipfile_name = path.join(tmpdir, path.splitext(genome_file)[0] + ".zip")
            zip(upload_dir, zipfile_name)
            move(zipfile_name, upload_dir)
        except IOError, e:
            print >>sys.stderr, "Moving the results failed: %s" % e
            sys.exit(2)

    rmtree(tmpdir)
    sys.exit(proc.returncode)

if __name__ == "__main__":
    main()
